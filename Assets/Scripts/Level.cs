﻿//using UnityEditor.SceneManagement;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    // Start is called before the first frame update

    [Range(0.1f, 10f)] [SerializeField] float gamespeed = 1f;
    [SerializeField] int BlocksDestroyed = 0;
    [SerializeField] TextMeshProUGUI text;

    private void Awake()
    {
        int num = FindObjectsOfType<Level>().Length;
        if (num > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
            return;


        }
        DontDestroyOnLoad(gameObject);


    }

    public void Start()
    {
        text.text = 0.ToString();


    }


    public void increase()
    {

        gamespeed = gamespeed + 0.1f;
        //me.timeScale = gamespeed;



    }
    void Update()
    {
        blockChecker();



    }

    private void blockChecker()
    {
        int blocks = FindObjectsOfType<destroyer>().Length;
        Debug.Log("the number of blocks destroyed are" + blocks);


        if (BlocksDestroyed == blocks)
        {
            int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

            SceneManager.LoadScene(currentSceneIndex + 1);
        }
    }

    public void destroyedBlock()
    {
        BlocksDestroyed++;
        text.text = BlocksDestroyed.ToString();


    }
    public void Reset()
    {
        Destroy(gameObject);


    }


}
