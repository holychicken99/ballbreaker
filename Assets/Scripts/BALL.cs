﻿using UnityEngine;

public class BALL : MonoBehaviour
{

    // config params
    [SerializeField] paddle paddle1;
    [SerializeField] float xPush = 2f;
    [SerializeField] float yPush = 15f;

    // state
    Vector2 paddleToBallVector;
    bool hasStarted = false;

    public BALL(paddle paddle1, float xPush, float yPush, Vector2 paddleToBallVector, bool hasStarted)
    {
        this.paddle1 = paddle1;
        this.xPush = xPush;
        this.yPush = yPush;
        this.paddleToBallVector = paddleToBallVector;
        this.hasStarted = hasStarted;
    }



    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasStarted)
        {
            LockBallToPaddle();
            LaunchOnMouseClick();
        }
        //var velocity = GetComponent<Rigidbody2D>().velocity;
        //var velX = velocity.x;
        //Mathf.Clamp(velocity.x, -25f, 25f);
        //Mathf.Clamp(velocity.y, -25f, 25f);



        //Debug.Log("the velocity is "+ velocity);


    }

    private void LaunchOnMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            hasStarted = true;
            GetComponent<Rigidbody2D>().velocity = new Vector2(xPush, yPush);

        }
    }

    private void LockBallToPaddle()
    {
        Vector2 paddlePos = new Vector2(paddle1.transform.position.x, paddle1.transform.position.y + 0.5F);
        transform.position = paddlePos;
    }
}