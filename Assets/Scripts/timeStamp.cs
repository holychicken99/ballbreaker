﻿using UnityEngine;

public class timeStamp : MonoBehaviour
{
    // Start is called before the first frame update
    [Range(0.1f, 10f)] [SerializeField] float gameSpeed = 1f;

    // Update is called once per frame
    public void increaseTime()
    {
        gameSpeed = gameSpeed + 0.2f;



    }
}
