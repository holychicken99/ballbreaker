﻿using UnityEngine;

public class destroyer : MonoBehaviour

{
    [SerializeField] Level level;
    [SerializeField] Sprite brokenBox;
    [SerializeField] GameObject vfx;



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (CompareTag("breakable"))
        {
            breakBox();

        }

    }

    private void breakBox()
    {
        Destroy(gameObject);

        vfx.GetComponent<Transform>().localScale = new Vector3(0.2f, 0.2f, 0f);




        Instantiate(vfx, transform.position, transform.rotation);

        level.increase();
        //GetComponent<SpriteRenderer>().sprite = brokenBox;
        //GetComponent<Transform>().localScale = new Vector3(0.3572953f, 0.3962477f, 1f);
        //Debug.Log("changed");




        level.destroyedBlock();
    }
}
